package sample;

//библиотека для вычисления значений функций, заданных в виде строк
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.Scanner;
import static java.lang.Math.*;

//класс для хранения значений первой и второй производной
class ResultDif {
    Double y_1;
    Double y_2;
}

//класс для хранения значений интеграла и погрешности интегрирования
class ResultInt{
    Double res;
    double d;
}

public class Main{

    //переменная для считывания значений, введенных пользователем с клавиатуры
    static Scanner scan = new Scanner(System.in);

    public static int menu(){
        int a;
        System.out.println("\nВыберите действие: ");
        System.out.println("1 - приближенный рассчет производных;");
        System.out.println("2 - приближенный рассчет интеграла;");
        System.out.println("3 - справка");
        System.out.println("0 - выход");
        //получение значения из консоли
        a = scan.nextInt();
        return a;
    }

    public static void main(String[] args) {
        int c = -1;
        //пока пользователь не введет "0"
        while (c != 0){
            //запрос у пользователя желаемого действия
            c = menu();
            switch (c){
                case 1:
                    calc_dif();
                    break;
                case 2:
                    calc_int();
                    break;
                case 3:
                    print_help();
                    break;
            }
        }
    }

    //реализация метода дифференцирования с помощью сплайнов
    public static void Spl_Integr(String fun, double a, double b, double x, int n, ResultDif ans){
        int i;
        double h, h1, h2, aa;
        double[] y;

        //проверка корректности значения n
        if (n<=0){
            System.out.println("Заданное значение n недопустимо");
            return;
        }

        //количество отрезков на один больше количества точек разбиения
        h = (b - a) / (n+1);
        //значения в точках разбиения плюс значения в точках a и b
        y = new double[n+2];

        aa = a;
        for (i = 0; i < y.length; i++) {
            //обработка исключения в случае невозможности вычисления введенной функции
            try {
                //вычисление значений функции
                y[i] = execute(fun, aa); 
            }catch (Exception e){
                System.out.println("Ошибка при вычислении значения функции");
                return; 
            }
            aa = aa + h; 
        }

        //определение позиции значения x
        i = (int) ((x - a) / h + h / 2);

        h1 = 2 * h;
        h2 = h * h;
        //случай, если x находится в начале интервала
        if (i == 0){
            ans.y_1 = (-3 * y[0] + 4 * y[1] - y[2]) / h1;
            //вычисление значения второй производной в случае, если известно достаточное количество значений функции
            ans.y_2 = (n > 1) ? (2 * y[0] - 5 * y[1] + 4 * y[2] - y[3]) / h2 : null;
        }
        //случай, если x находится в середине интервала
        if (i > 0 && i < n+1){
            ans.y_1 = (-y[i-1] + y[i+1]) / h1;
            ans.y_2 = (y[i-1] - 2 * y[i] + y[i+1]) / h2;
        }
        //случай, если x находится в конце интервала
        if (i == n+1){
            ans.y_1 = (y[n-1] - 4 * y[n] + 3 * y[n+1]) / h1;
            //вычисление значения второй производной в случае, если известно достаточное количество значений функции
            ans.y_2 = (n > 1) ? (-y[n-2] + 4 * y[n-1] - 5 * y[n] + 2 * y[n+1]) / h2 : null;
        }
    }

    //реализация метода Симпсона для вычисления приближенного значения определенного инерграла
    public static void Simpson(String fun, double a, double b, double d, ResultInt ans){
        double f0, f1, s, s1, s2, h, x1, x2;
        int i, n;

        //проверка корректности значения d
        if (d<=0){
            System.out.println("Заданное значение d недопустимо");
            return;
        }

        //обработка исключения в случае невозможности вычисления введенной функции
        try {
            //вычисление значений функции на границах интервала
            f0 = execute(fun, a);
            f1 = execute(fun, b);
        }catch (Exception e){
            System.out.println("Ошибка при вычислении значения функции");
            return;
        }
        //постоянное слагаемое в формуле Симпсона
        s = f0 - f1;
        //приближенное значение при n = 2
        s1 = (b - a) * (f0 + f1 + 4 * execute(fun,(a + b) / 2)) / 6;
        n = 2;
        do {
            h = (b - a) / n;
            x1 = a + h / 2;
            x2 = a + h;
            s2 = s;
            //вычисление суммы значений функции в промежуточных точках
            for (i = 1; i <= n; i++) {
                //обработка исключения в случае невозможности вычисления введенной функции
                try {
                    s2 = s2 + 4 * execute(fun, x1) + 2 * execute(fun, x2);
                }catch (Exception e){
                    System.out.println("Ошибка при вычислении значения функции");
                    return;
                }
                x1 = x1 + h;
                x2 = x2 + h;
            }
            s2 = s2 * h / 6;
            //рассчет погрешности при текущем n
            ans.d = abs(s1 - s2) / 15;
            s1 = s2;
            n = n * 2;
        //пока текущая погрешность превышает максимально допустимую
        } while (ans.d > d);
        ans.res = s1;
    }

    //вычисление значения функции, переданной в виде строки
    public static double execute(String fun, double x){
        //парсинг введенной функции
        Expression e = new ExpressionBuilder(fun)
                .variables("x")
                .build()
                .setVariable("x", x);
        //вычисление значения полученной функции
        return e.evaluate();
    }

    //приближенное вычисление производных
    public static void calc_dif(){
        String fun;
        double a, b, x;
        int n;
        //переменная для хранения значений первой и второй производных
        ResultDif ans_dif = new ResultDif();

        //ввод исходных данных
        System.out.print("Введите функцию: ");
        //очистка буфера
        scan.nextLine();
        fun = scan.nextLine();
        System.out.print("Введите нижнюю и верхнюю границы интервала: ");
        a = scan.nextDouble();
        b = scan.nextDouble();
        System.out.print("Введите точку дифференцирования: ");
        x = scan.nextDouble();
        System.out.print("Введите число точек дифференцирования: ");
        n = scan.nextInt();

        Spl_Integr(fun, a, b, x, n, ans_dif);
        if (ans_dif.y_1 != null) {
            System.out.println("Первая производная: " + ans_dif.y_1);
        }
        else {
            System.out.println("При заданных параметрах рассчет первой производной невозможен");
        }
        if (ans_dif.y_2 != null) {
            System.out.println("Вторая производная: " + ans_dif.y_2);
        }
        else {
            System.out.println("При заданных параметрах рассчет второй производной невозможен");
        }
    }

    //приближенное вычисление интеграла
    public static void calc_int(){
        String fun;
        double a, b, d;
        //переменная для хранения значений интеграла и погрешности интегрирования
        ResultInt ans_int = new ResultInt();

        //ввод исходных данных
        System.out.print("Введите функцию: ");
        //очистка буфера
        scan.nextLine();
        fun = scan.nextLine();
        System.out.print("Введите нижнюю и верхнюю границы интервала: ");
        a = scan.nextDouble();
        b = scan.nextDouble();
        System.out.print("Введите максимально допустимую погрешность: ");
        d = scan.nextDouble();

        Simpson(fun, a, b, d, ans_int);
        if (ans_int.res != null) {
            System.out.println("Значение интеграла: "+ans_int.res);
            System.out.println("Погрешность вычисления: "+ans_int.d);
        }
        else {
            System.out.println("При заданных параметрах рассчет значения инеграла невозможен");
        }
    }

    public static void print_help(){
        System.out.println("-------------------\nПоддерживаемые функции: ");
        System.out.println("abs: абсолютное значение\n" +
                "acos: арккосинус\n" +
                "asin: арксинус\n" +
                "atan: арктангенс\n" +
                "cbrt: кубический корень\n" +
                "ceil: округление в большую сторону\n" +
                "cos: косинус\n" +
                "cosh: гиперболический косинус\n" +
                "exp: экспонента (e^x)\n" +
                "floor: округление в меньшую сторону\n" +
                "log: натуральный логарифм\n" +
                "log10: десятичный логарифм\n" +
                "log2: двоичный логарифм\n" +
                "sin: синус\n" +
                "sinh: гиперболический синус\n" +
                "sqrt: квадратный корень\n" +
                "tan: тангенс\n" +
                "tanh: гиперболический тангенс\n" +
                "signum: сигнум");
        System.out.println("-------------------");
    }
}